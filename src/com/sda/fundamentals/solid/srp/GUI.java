package com.sda.fundamentals.solid.srp;

public class GUI {
    public static void main(String[] args) {
        Rectangle rectangle = new Rectangle();
        rectangle.computeArea("yards");

        RectangleView rectangleView = new RectangleView();
        rectangleView.draw();
    }
}
