package com.sda.fundamentals.solid;

import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RefactoringExample {

    private static final double ONE_NANO_IN_MS = 1.0E-6;

    public static void main(String[] args) {
        var startTime = LocalTime.now();
        stringsToInts(List.of("1", "2", "3"));
        var durationInNanoSeconds = LocalTime.now()
                .minus(startTime.getNano(), ChronoUnit.NANOS)
                .getNano();
        System.out.printf("%nOriginal method took %.2f ms to run!", convertNanosToMs(durationInNanoSeconds));

        var startTime2 = LocalTime.now();
        stringsToIntsRefactored(List.of("1", "2", "3"));
        var durationInNanoSeconds2 = LocalTime.now()
                .minus(startTime2.getNano(), ChronoUnit.NANOS)
                .getNano();
        System.out.printf("%nRefactored method took %.2f ms to run!", convertNanosToMs(durationInNanoSeconds2));
    }

    private static double convertNanosToMs(int nanos) {
        return nanos * ONE_NANO_IN_MS;
    }

    private static List<Integer> stringsToInts(List<String> strings) {
        if (strings != null) {
            List<Integer> integers = new ArrayList<>();
            for (String s : strings) {
                integers.add(Integer.parseInt(s));
            }
            return integers;
        } else {
            return null;
        }
    }

    private static List<Integer> stringsToIntsStep1(List<String> strings) {
        // early return
        if (strings == null) {
            return null;
        }

        List<Integer> integers = new ArrayList<>();
        for (String s : strings) {
            integers.add(Integer.parseInt(s));
        }
        return integers;
    }

    private static List<Integer> stringsToIntsStep2(List<String> strings) {
        if (strings == null) {
            return Collections.emptyList(); // avoid returning nulls
        }

        List<Integer> integers = new ArrayList<>();
        for (String s : strings) {
            integers.add(Integer.parseInt(s));
        }
        return integers;
    }

    private static List<Integer> stringsToIntsStep3(List<String> strings) {
        if (strings == null) {
            return Collections.emptyList();
        }

        // use lambda 8 functionalities on streams
        return strings.stream()
                .map(s -> Integer.parseInt(s))
                .collect(Collectors.toList());
    }

    private static List<Integer> stringsToIntsRefactored(List<String> strings) {
        // use inline if and reduce the number of return statements
        return strings == null ?
                Collections.emptyList() :
                strings.stream()
                        .map(Integer::parseInt) // this is called method reference <=> (s) -> Integer.parseInt(s)
                        .collect(Collectors.toList());
    }

}