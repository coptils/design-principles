package com.sda.fundamentals.solid.interfacesegregation.refactored;

public interface RobotsWithArms {
    void rotateArms();
}
