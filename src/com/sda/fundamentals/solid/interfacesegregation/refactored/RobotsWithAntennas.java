package com.sda.fundamentals.solid.interfacesegregation.refactored;

public interface RobotsWithAntennas {
    void wiggleAntennas();
}
