package com.sda.fundamentals.solid.interfacesegregation.refactored;

public interface RobotsThatCanSpinAround {
    void spinAround();
}
