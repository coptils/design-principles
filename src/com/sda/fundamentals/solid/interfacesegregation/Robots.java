package com.sda.fundamentals.solid.interfacesegregation;

public interface Robots {
    void spinAround();
    void rotateArms();
    void wiggleAntennas();
}
