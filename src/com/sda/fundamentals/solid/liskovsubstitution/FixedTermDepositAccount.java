package com.sda.fundamentals.solid.liskovsubstitution;

import java.math.BigDecimal;

// violates the Liskov Sustitution Principle
// Subtypes must be substitutable for their base types.
public class FixedTermDepositAccount extends Account {
    @Override
    public void deposit(BigDecimal amount) {
        // Deposit into this account
        System.out.println("Deposit: " + amount);
    }

//    @Override
//    protected void withdraw(BigDecimal amount) throws RuntimeException {
//        throw new UnsupportedOperationException("Withdrawals are not supported by FixedTermDepositAccount!!");
//    }

    @Override
    protected void withdraw(BigDecimal amount) {
        throw new UnsupportedOperationException("Withdrawals are not supported by FixedTermDepositAccount!!");
    }
}