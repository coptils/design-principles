package com.sda.fundamentals.solid.liskovsubstitution;

import java.math.BigDecimal;
import java.nio.charset.MalformedInputException;

public class TestBankingApp {
    public static void main(String[] args) throws MalformedInputException {
        Account myFixedTermDepositAccount = new FixedTermDepositAccount();
        myFixedTermDepositAccount.deposit(new BigDecimal(1000.00));

        BankingAppWithdrawalService withdrawalService = new BankingAppWithdrawalService(myFixedTermDepositAccount);
        withdrawalService.withdraw(new BigDecimal(100.00));
    }
}
