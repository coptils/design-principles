package com.sda.fundamentals.solid.liskovsubstitution;

import java.math.BigDecimal;
import java.nio.charset.MalformedInputException;

public class BankingAppWithdrawalService {
    private Account account;

    public BankingAppWithdrawalService(Account account) {
        this.account = account;
    }

    public void withdraw(BigDecimal amount) throws MalformedInputException {
        account.withdraw(amount);
    }

}