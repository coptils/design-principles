package com.sda.fundamentals.solid.liskovsubstitution.refactored;

import java.math.BigDecimal;

public class SavingAccountsAccount extends WithdrawableAccount {
    @Override
    protected void deposit(BigDecimal amount) {

    }

    @Override
    protected void withdraw(BigDecimal amount) {

    }
}
