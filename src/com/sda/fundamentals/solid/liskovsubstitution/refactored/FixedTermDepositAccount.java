package com.sda.fundamentals.solid.liskovsubstitution.refactored;

import java.math.BigDecimal;

public class FixedTermDepositAccount extends Account {
    @Override
    protected void deposit(BigDecimal amount) {
        // Deposit into this account
    }
}