package com.sda.fundamentals.solid.liskovsubstitution.refactored;

import java.math.BigDecimal;

public class TestBankingApp {
    public static void main(String[] args) {
        WithdrawableAccount savingDepositAccounts = new SavingAccountsAccount();
        savingDepositAccounts.deposit(new BigDecimal(1000.00));

        BankingAppWithdrawalService withdrawalService = new BankingAppWithdrawalService(savingDepositAccounts);
        withdrawalService.withdraw(new BigDecimal(100.00));
    }
}
