package com.sda.fundamentals.solid.liskovsubstitution.refactored;

import java.math.BigDecimal;

public abstract class Account {
    protected abstract void deposit(BigDecimal amount);
}