package com.sda.fundamentals.solid.ocp;

public class EmergencyRoomProcess {
    public static void main(String[] args) {
        HospitalManagement ERDirector = new HospitalManagement();
        Employee jhon = new Nurse(1, "Jhon", "emergency", true);

        ERDirector.callUpon(jhon);

        Employee suzan = new Doctor(2, "Suzan", "emergency", true);
        ERDirector.callUpon(suzan);

        Employee andrew = new SecurityGuardian(3, "Andrew", "emergency", true);
        ERDirector.callUpon(andrew);

        Employee cris = new Manager(4, "Chris", "emergency", true);
        ERDirector.callUpon(cris);
    }
}
