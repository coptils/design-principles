package com.sda.fundamentals.solid.ocp;

public class Manager extends Employee {

    public Manager(int id, String name, String department, boolean working) {
        super(id, name, department, working);
        System.out.println("Manager in action");
    }

    @Override
    public void performDuties() {
        goToMeeting();
    }

    private void goToMeeting() {
        System.out.println("Manager in meeting");
    }
}
