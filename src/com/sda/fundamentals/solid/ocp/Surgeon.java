package com.sda.fundamentals.solid.ocp;

public class Surgeon extends Employee {

    public Surgeon(int id, String name, String department, boolean working) {
        super(id, name, department, working);
    }

    @Override
    public void performDuties() {
        //
    }
}
