package com.sda.fundamentals.solid.ocp;

public class HospitalManagementBadDesign {

    public void callUpon(Employee employees) {
        if (employees instanceof Doctor) {
            prescribeMedicine();
            diagnosePatients();
        }
        if (employees instanceof Manager) {
            goToMeeting();
        }
        if (employees instanceof Nurse) {
            checkVitalSigns();
            drawBlood();
            cleanPatientArea();
        }
        if (employees instanceof SecurityGuardian) {
            checkRoom();
        }

        if (employees instanceof Director) {
             checkEmployees();
        }
    }

    public void prescribeMedicine() {
        System.out.println("Prescribing medicine");
    }

    public void diagnosePatients() {
        System.out.println("Diagnosing Patient");
    }

    private void goToMeeting() {
        System.out.println("Manager in meeting");
    }

    public void checkVitalSigns() {
        System.out.println("Checking vitals");
    }

    public void drawBlood() {
        System.out.println("Drawing blood");
    }

    public void cleanPatientArea() {
        System.out.println("Cleaning patient area");
    }

    public void checkRoom() {
        System.out.println("Checking room");
    }

    public void checkEmployees() {
        System.out.println("check employees");
    }

}
