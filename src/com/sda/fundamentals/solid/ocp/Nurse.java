package com.sda.fundamentals.solid.ocp;

public class Nurse extends Employee {
    public Nurse(int id, String name, String department, boolean working) {
        super(id, name, department, working);
        System.out.println("Nurse in action...");
    }

    @Override
    public void performDuties() {
        checkVitalSigns();
        drawBlood();
        cleanPatientArea();
    }

    public void checkVitalSigns() {
        System.out.println("Checking vitals");
    }

    public void drawBlood() {
        System.out.println("Drawing blood");
    }

    public void cleanPatientArea() {
        System.out.println("Cleaning patient area");
    }

}
