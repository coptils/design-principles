package com.sda.fundamentals.solid.ocp;

public class SecurityGuardian extends Employee {
    public SecurityGuardian(int id, String name, String department, boolean working) {
        super(id, name, department, working);
        System.out.println("Security guardian in action...");
    }

    @Override
    public void performDuties() {
        checkRoom();
    }

    public void checkRoom() {
        System.out.println("Checking room");
    }

}
