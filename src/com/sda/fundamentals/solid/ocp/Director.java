package com.sda.fundamentals.solid.ocp;

public class Director extends Employee {

    public Director(int id, String name, String department, boolean working) {
        super(id, name, department, working);
    }

    @Override
    public void performDuties() {

    }

    public void checkEmployees() {
        System.out.println("check employees");
    }
}
