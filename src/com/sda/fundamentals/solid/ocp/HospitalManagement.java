package com.sda.fundamentals.solid.ocp;

public class HospitalManagement {

    public void callUpon(Employee employees) {
        employees.performDuties();
    }

}
