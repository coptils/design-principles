package com.sda.fundamentals.solid.di.refactored;

// high level module/class
public class OrderService {

    // DIP = abstractions should not depend on details, details should depend on abstractions
    private IOrderRepository repo;

    public void getOrderByUserId(){
        repo.getById();
    }
}
