package com.sda.fundamentals.solid.di.refactored;

// low level module/class - contains implementation details
public class OrderRepository implements IOrderRepository {
    
    public Order getById() {
        // db connection
        // db string
        // result set - rezultatul din baza de date
        return new Order();
    }

}
