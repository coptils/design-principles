package com.sda.fundamentals.solid.di.refactored;

// abstraction - connects the low-level class with high-level class
public interface IOrderRepository {

    Order getById();

}
