package com.sda.fundamentals.solid.di;

// clasa high-level
public class OrderService {

    // violates DIP => it depends on details not on abstractions
    private OrderRepository repo;

    public void m(){
        repo.getById();
        // compute smth on order instance
    }
}
