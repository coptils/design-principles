# design-principles

# Software Craftsmanship Manifesto
https://manifesto.softwarecraftsmanship.org 

# Design Principles in Images
https://medium.com/backticks-tildes/the-s-o-l-i-d-principles-in-pictures-b34ce2f1e898 

## Liskov Substitution Principle
https://medium.com/@malamsyah/liskov-subtitution-principle-e6152584beee

# Clean Code
https://drive.google.com/file/d/1n398LBSdsYO32y5yayohzV9elY2_kPJ5/view 

# JIT
https://www.geeksforgeeks.org/just-in-time-compiler/

# Reflection in Java
https://www.geeksforgeeks.org/reflection-in-java/

